<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script>var h, m, s, cs, ms, countStarted;
    h = 0;
    m = 0;
    s = 0;
    cs = 0;
    countStarted = false;
    var refreshIntervalId = null;
    var start = null;

    function stopcount()
    {
        countStarted =false;
        clearInterval(refreshIntervalId);
        document.getElementById('stopped').style.visibility = "visible";
        document.getElementById("stopped").innerHTML = czas;
    }</script>
</head>
<body>

<div id="box">
    <button id="click">Start!</button>
    <button id="other">Stop!</button>
    <div id="timebox" style="visibility: hidden;">
        <p id="time"></p>
    </div>
    <div id="stopped" style="visibility: hidden;">
        <p id="timed">Stopped</p>
    </div>
</div>

</body>
</html>
