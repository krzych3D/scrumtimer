package pl.sda.helllo.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppController {

    @Autowired
    private IUserModel IUserModel;

    @RequestMapping(value = "/timer", method = RequestMethod.GET)
    public ModelAndView printWelcome(ModelMap model) {
//        model.addAttribute("name", IUserModel.getName());
        ModelAndView model1 = new ModelAndView();
        model1.setViewName("timer");
        return model1;
    }

//    @RequestMapping(value = "/hello/{name:.+}", method = RequestMethod.GET)
//    public ModelAndView hello(@PathVariable("name") String name) {
//        IUserModel.setName(name);
//        ModelAndView model = new ModelAndView();
//        model.setViewName("hello");
//        model.addObject("name", name);
//        return model;
//    }

}
